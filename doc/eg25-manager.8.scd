eg25-manager(8)

# NAME
eg25-manager - a daemon for managing the Quectel EG25 modem found on the
Pine64 PinePhone.

# SYNOPSIS
*eg25-manager* [-v] [-c config_file]

# OPTIONS
*-v*
	Show the version number and quit.
*-c*
	User configuration file, defaults to the device configuration file in
	/etc/eg25-manager.

# FILES
Configurations are loaded from:
	- *@eg25_confdir@/<compatible>.toml*: User-provided overrides (optional)
	- *@eg25_datadir@/<compatible>.toml*: Default configuration (required)

eg25-manager will search these folders for files named after the value of the
compatible device-tree property (with the .toml file extension) and use the
first matching file in each directory. If no matching default configuration is
found, eg25-manager will exit with an error message.

Values from the user-provided overrides will take priority over values stored in
the default configuration. Only changed values must be stored as user overrides,
so eg25-manager can fall back to the default configuration as often as possible.

The file names eg25-manager will check can be listed using:
```
xargs -0 printf '%s.toml\\n' < /proc/device-tree/compatible
```

# SEE ALSO
*eg25-manager*(5) *ModemManager*(8) *ofono*(8)
