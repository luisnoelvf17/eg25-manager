eg25-manager(5)

# NAME
eg25-manager configuration file format

# SYNOPSIS
eg25-manager uses toml formatted files for configuration.

Configurations are loaded from:
	- *@eg25_confdir@/<compatible>.toml*: User-provided overrides (optional)
	- *@eg25_datadir@/<compatible>.toml*: Default configuration (required)

# SECTION: manager
General settings for eg25-manager.

*poweron_delay* int (microseconds)
	Delay between de-asserting RESET and starting the PWRKEY sequence.

# SECTION: suspend
Settings for how to handle suspend on the system where eg25-manager is running.

*boot_timeout* int (seconds)
	Prevent the system from suspending for boot_timeout seconds to allow the
	modem to fully boot.

	Default: 120 if unset or zero.

*recovery_timeout* int (seconds)
	Amount of time to wait for the modem to reappear after suspend. If the
	timeout is reached the modem's USB connection will be reset.

	Default: 9 if unset or zero.

# SECTION: at
AT commands to send when different events happen, and where to send them to.

Each command has 4 possible elements:
	- *cmd*: the AT command itself, which will be translated to "AT+`cmd`"
	- *subcmd*: the subcommand in case a single AT command can be used to
	  change multiple parameters, such as QCFG
	- *value*: the command's argument(s), usually used to set the value of a
	  specific parameter
	- *expect*: the expected return value; the command is first executed
	  without any value in order to query the current state. This state is
	  then compared to the *expect* string; if they don't match, the command
	  is then executed with value *expect* in order to set the parameter to
	  the expected value
A command can have *expect* OR *value* configured, but it shouldn't have both

*NOTE:* If a command sequence is configured in an override file, the default
commands won't be loaded from the system configuration. The default commands
should be copied into the override file when changing them.

*uart* string
	The serial port to use for sending AT commands to the modem.

*configure* List of commands
	AT commands to send to the modem when it is first started.

*suspend* List of commands
	AT commands to send to the modem before the system suspends.

*resume* List of commands
	AT commands to send to the modem after the system resumes from suspend.

*reset* List of commands
	AT commands to send to the modem if resetting the usb port fails.

# SECTION: gnss
Settings for uploading AGPS assistance data to the modem.

*enabled* boolean
	Enable or disable uploading AGPS data to the modem

*url* string
	The directory on the server that contains the assistance files

	Example: https://xtrapath4.izatcloud.net

*file* string
	The name of the assistance file on the server.

	Example: xtra2.bin

# SECTION: gpio
The *gpio* section defines the GPIO pins to use for different modem functions.
These settings should only be changed when porting eg25-manager to a new device;
for this reason they aren't documented here.

# EXAMPLES
Print the firmware version every time the phone wakes from suspend:
```
[at]
resume = [
    { cmd = "QGMR" },
]
```

Disable uploading AGPS data to the modem:
```
[gnss]
enabled = false
```

# SEE AlSO
*eg25-manager*(8)
