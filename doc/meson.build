#
# Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

scdoc = dependency('scdoc', native: true, required: false)
if scdoc.found()
  scdoc_prog = find_program(scdoc.get_variable('scdoc'), native: true)

  foreach section: [5, 8]
    name = 'eg25-manager'
    out = '@0@.@1@'.format(name, section)

    preprocessed = configure_file(
      input: '@0@.scd'.format(out),
      output: '@BASENAME@.preprocessed',
      configuration: {
        'eg25_confdir': eg25_confdir,
        'eg25_datadir': eg25_datadir,
        }
      )

    custom_target(
      out,
      output: out,
      input: preprocessed,
      command: ['sh', '-c', '@0@ < @INPUT@'.format(scdoc_prog.full_path())],
      capture: true,
      install: true,
      install_dir: join_paths(get_option('mandir'), 'man@0@'.format(section)))
  endforeach
endif
