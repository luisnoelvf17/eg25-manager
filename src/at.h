/*
 * Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "manager.h"

typedef struct AtCommand {
    char *cmd;
    char *subcmd;
    char *value;
    char *expected;
    void (*callback)(struct EG25Manager *manager, const char *response);
    int retries;
} AtCommand;

int at_init(struct EG25Manager *manager, toml_table_t *config[]);
void at_destroy(struct EG25Manager *manager);

void at_process_result(struct EG25Manager *manager,
                       const char         *response);
void at_next_command(struct EG25Manager *manager);
gboolean at_send_command(struct EG25Manager *manager);
int at_append_command(struct EG25Manager *manager,
                      const char         *cmd,
                      const char         *subcmd,
                      const char         *value,
                      const char         *expected,
                      void              (*callback)
                                        (struct EG25Manager *manager,
                                         const char *response));

void at_sequence_configure(struct EG25Manager *manager);
void at_sequence_suspend(struct EG25Manager *manager);
void at_sequence_resume(struct EG25Manager *manager);
void at_sequence_reset(struct EG25Manager *manager);
