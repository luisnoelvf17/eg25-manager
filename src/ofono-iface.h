/*
 * Copyright (C) 2020 Oliver Smith <ollieparanoid@postmarketos.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "manager.h"

void ofono_iface_init(struct EG25Manager *data, toml_table_t *config[]);
void ofono_iface_destroy(struct EG25Manager *data);
