/*
 * Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "manager.h"
#include "toml.h"

/*
 * Helper functions for parsing config files: each function retrieves the
 * value for key `key`, with the user config file having priority over the
 * default config file. The values are stored in `result`.
 *
 * They all return TRUE if the value was found, FALSE otherwise.
 */

gboolean config_get_bool(toml_table_t **config, const gchar *key, gboolean *result);
gboolean config_get_int(toml_table_t **config, const gchar *key, gint *result);
gboolean config_get_uint(toml_table_t **config, const gchar *key, guint *result);
gboolean config_get_string(toml_table_t **config, const gchar *key, gchar **result);
gboolean config_get_array(toml_table_t **config, const gchar *key, toml_array_t **result);
gboolean config_get_table(toml_table_t **config, const gchar *key, toml_table_t **result);
